run:
	elm reactor

compile:
	elm make build/Order.elm --output order.html
	elm make build/AdjOrder.elm --output adjorder.html
