#+title: Building a Transition System based Experiment in Elm
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup

* Introduction

Bubblesort is a sorting algorithm that performs repeated swap
operations in a particular manner to sort a given list.  In the
algodynamics approach we start with the very basic transition system
for swapping two numbers in a list and then build on on top of that
system eventually leading to the bubblesort algorithm.

The intermediate systems involved in developing the final bubblesort
algorithm are as follows:

- 1. Swap Machine :: Swapping any two numbers in a list.

- 2. Order Machine :: Swapping numbers that are out of order.

- 3. Adjacent Order Machine :: /Ordering/ adjacent numbers.
 
- 4. Bubble Machine :: Step-by-ste iteration through the list and
     ordering adjacent numbers.  Iteration can be reset at any point
     and does not need to terminate.

- 5. Bubblesort Machine :: Step-by-step iteration through the list
     following the bubblesort algorithm.

We start by taking the example of /Order Machine/ to demonstrate how
to build a transition system based experiment.  This is followed by
the /Adjacent Order Machine/ where we discuss how we leaverage the
already existing order machine to build the adjacent order machine.

 In this document we discuss the following:

- 1. [[Basic Transition Systems][Basic Transition Systems]] :: Implementing the transition system
     that we define on paper as part of designing the experiment.

- 2. [[Core Functions][Core Functions]] :: Functions that are used to compose the
     transition function.

- 3. [[Experiment Level Transition System][Experiment Level Transition System]] :: The experiment level system
     is a composition of the basic system along with additional states
     required for the user interface.

- 4. [[Incrementation Changes for Successive Refinement][Incremental changes for succesive refinement]] :: In this section
     we discuss the /Adjacent Order Machine/ which is a refinment of
     the /Order Machine/.

* Basic Transition Systems
  
Components of a Transition System:

1. State
2. Action
3. Transition Function
4. Output

The state and action varies according the transition system but the
transition functions are composed of a set of common function.  We
define these functions in a =Core= module.

** Core Functions

All functions required for systems that are defined as successive
refinements that lead to some algorithm are defined in a common
module.

*** Swap

The swap function exchanges the numbers at index =i= and =j= in the
given list.

If =lst[i] = x= and =lst[j] = y= then after applying the swap function to
=lst= we get a list such that:

=lst[i] = y= and =lst[j] = x=.

#+NAME: swap
#+BEGIN_SRC elm
swap : Int -> Int -> List Int -> List Int
swap i j array =
    swapAt i j array
#+END_SRC


*** Order

The order function swaps numbers at index =i= and =j= (i < j) in the list
=lst= if the numbers at i and j are out of order.

The order function assumes that i < j.

#+NAME: order
#+BEGIN_SRC elm
order: Int -> Int -> List Int -> List Int
order i j lst =    
    if (outOfOrder i j lst) then
        swap i j lst
    else
        lst
#+END_SRC


*** Out of Order

Check if the values in the given indices =i= and =j= of the list are
out of order.

#+NAME: out-of-order
#+BEGIN_SRC elm
outOfOrder: Int -> Int -> List Int -> Bool
outOfOrder i j lst =
    let ai = getAt i lst
        aj = getAt j lst
    in
        case (ai, aj) of
            (Just x, Just y) ->
                (i < j) && (x > y)
            _ -> False
#+END_SRC


*** Adjacent Order

The adjacent order function performs =order='s the numbers at index
=i= and =i+1= (if i and i+1 are valid indices).

#+NAME: adjorder
#+BEGIN_SRC elm
adjorder: Int -> List Int -> List Int
adjorder i lst = 
    if i < (List.length lst) - 1 then 
        order i (i + 1) lst 
    else 
        lst
#+END_SRC


*** Tangle the Core Module :noexport:

#+BEGIN_SRC elm :eval no :noweb yes :mkdirp yes :tangle ../build/Core.elm
module Core exposing (..)

import List.Extra exposing (swapAt, getAt)


<<swap>>

<<order>>

<<out-of-order>>

<<adjorder>>
#+END_SRC

* Order Machine

** State

State of a transition system consists of all the objects that are
relevant to computations involved in the transition system.

We define a =OrderMachineState= type in Elm that defines the state of
the system.

In the /Order Machine/ system, the state consists of a list of
numbers.

#+NAME: order-machine-state
#+BEGIN_SRC elm
type alias OrderMachineState = List Int
#+END_SRC

** Action

The =Msg= type defines the set of inputs to the system.  These
messages are triggered by the =Elm Runtime= that executes in the
browser.  when a user interacts with the interface by clicking on any
input button, the corresponding message that is defined in the view is
triggered and it is handled by the =udpate= function.

#+NAME: order-mc-action
#+BEGIN_SRC elm
type OrderMachineAction = Order Int Int
#+END_SRC

** Transition Functions

The update function maps the messages in the system to the function
that actually performs the transition.  In this function we map each
=Action= variant to a transition function.

The /Order Machine/ consists of the following input messages:

|-------------+---------------------------------------------------|
| Action      | Description                                       |
|-------------+---------------------------------------------------|
| =Order i j= | Order the numbers at i and j indices in the list. |
|-------------+---------------------------------------------------|

#+NAME: transition
#+BEGIN_SRC elm
orderUpdate: OrderMachineAction -> OrderMachineState -> OrderMachineState
orderUpdate action state =
    case action of
        Order i j -> order i j state
#+END_SRC

   
** Output

The output of the transition system is Html code that is rendered by
the browser.

The view needs to trigger messages that are not a part of the /Order
Machine's/ basic transition system.  These messages correspond to the
transition system that is required to make the interface work.  So, we
will define the view later when we define that system.  

The top level system will consist of the transition system for the
/Order Machine/ as well as the some additional machinery required for
the interface.


** Experiment Level Transition System

The Experiment Level Transition system is composed of the /Basic
Transition System/ and additional actions to handle the input
interface and output.

*** State

|------------+--------------------------------------------------------------------------|
| Field      | Desciription                                                             |
|------------+--------------------------------------------------------------------------|
| lst        | The =Order Machine State= represents the state space of /Order Machine/. |
| selections | Used to keep track of selected indices.                                  |
|------------+--------------------------------------------------------------------------|

#+NAME: state-exp
#+BEGIN_SRC elm
type alias State =
    { lst: OrderMachineState
    , selections: Selections
    }
#+END_SRC


*** Selections

Selections are used to keep track of the indices that the user selects
by clicking on the corresponding list items.

|------------------+-----------------------------------------------|
| Variant          | Desctiption                                   |
|------------------+-----------------------------------------------|
| NoneSelected     | No list item is selected.                     |
| OneSelected i    | One list item at index i is selected.         |
| BothSelected i j | Two list items at index i and j are selected. |
|------------------+-----------------------------------------------|

#+NAME: selections
#+BEGIN_SRC elm
type Selections
    = NoneSelected
    | OneSelected Int
    | BothSelected Int Int
#+END_SRC

**** Select

When the user clicks on an unselected list item the state of
selections is updated according to the current state of selections as
follows:

- No Item is Selected :: If no item in the list has been selected
     before clicking on an item (with index =i=), then we update the
     state of =selections= to =OneSelected i=.

- One Item is Selected :: If one item (i) has already been selected
     before clicking on an unselected item with index =j= then we
     update the selections state to =BothSelected p q= where =p = (min
     i j)= and =q = (max i j)=.

- Two items are Selected :: If two items are already selected then we
     do not change the state.  This is due to the fact that we do not
     need more that two items for any list operation in our system.
     We require the user to first unselect/deselect one or more
     selected items before selecting another item.

#+NAME: select
#+BEGIN_SRC elm
select : Int -> Selections -> Selections
select i selections =
    case selections of
        NoneSelected ->
            OneSelected i
        OneSelected j ->
            BothSelected (min i j) (max i j)
        BothSelected j k ->
            BothSelected j k
#+END_SRC

**** Deselect

When the user clicks on a selected item, the corresponding action of
deselecting the item and updating the state accordingly is handled by
the =deselect= function.

We need to handle three cases that correspond to three states that the
=selections= can be in before the user clicks on the item:

- No Item is Selected :: This is an unreachable state because in order
     to click on a selected item, there must exists atleast one
     selected item.

- Only One Item is Selected :: In this case we update the selections
     state to =NoneSelected=.

- Two Items are Selected :: In this case we update the selections
     state to =OneSelected=.

#+NAME: deselect
#+BEGIN_SRC elm
deselect : Int -> Selections -> Selections
deselect i selections =
    case selections of

        NoneSelected ->
            NoneSelected

        OneSelected j ->
            if (i == j) then 
                NoneSelected
            else
                OneSelected j

        BothSelected j k ->
            if (i == j) then
                OneSelected k
            else
                if (i == k) then
                    OneSelected j
                else 
                    BothSelected j k
#+END_SRC


**** Selection Check Predicate

The =isSelected= function checks if the item at given index =i= is
selected or not.

#+NAME: is-selected
#+BEGIN_SRC elm
isSelected : Int -> Selections -> Bool
isSelected index selections =
    case selections of
        NoneSelected ->
            False
        OneSelected j ->
            if j == index then True else False
        BothSelected j k ->
            if (j == index || k == index) then True else False
#+END_SRC


**** Select Adjacent Items

When the user clicks on an unselected list item in the /Order Adjacent
Machine/ the =adjselect= function is triggered.  This function is a
composition of two =select= functions that select the items at index
=i= and =i+1=.

#+NAME: select-adj
#+BEGIN_SRC elm
adjselect : Int -> Selections -> Selections
adjselect i selections = 
    selections |> select i |> select (i+1)
#+END_SRC

**** Deselect Adjacent Selection

=deselect= function is composed of two =deselect= functions that undo
the selection of items at index =i= and =i+1=.

#+NAME: deselect-adj
#+BEGIN_SRC elm
adjdeselect : Int -> Selections -> Selections
adjdeselect i selections =
    selections |> deselect i |> deselect (i+1)
#+END_SRC

*** Get the list of Selected Indices

=selectionsList= function converts the =Selections= datatype to a list
of selected indices.  This required when we want to send the indices
to the list visualization function that does not use the =Selections=
type.

#+NAME: selection-list
#+BEGIN_SRC elm
selectionsList : Selections -> List Int
selectionsList selections =
    case selections of
       NoneSelected -> []
       OneSelected i -> [i]
       BothSelected i j -> [i, j]
#+END_SRC


*** Tangle Selections Module :noexport:


#+BEGIN_SRC elm :eval no :noweb yes :mkdirp yes :tangle ../build/Selection.elm
module Selection exposing (..)

<<selections>>

<<select>>

<<deselect>>

<<is-selected>>

<<select-adj>>

<<deselect-adj>>

<<selection-list>>
#+END_SRC

*** Actions

The actions in the transition system for Order Machine Experiment
consist of actions for the Order Machine along with actions
corresponding to the buttons in the interface.

|-------------+-------------------------------------------------|
| Action      | Description                                     |
|-------------+-------------------------------------------------|
| OrderAction | Actions for the Order Machine                   |
| Select      | Action to select an item in the list.           |
| Deselect    | Action to deselect a selected item in the list. |
|-------------+-------------------------------------------------|


#+NAME: order-exp-action
#+BEGIN_SRC elm
type Action = OrderAction OrderMachineAction
            | Select Int
            | Deselect Int
#+END_SRC

*** Transition Function

The transition function for the experiment maps the actions to
corresponding functions that update the state according to the action.

- OrderAction :: The OrderAction is triggered when the user clicks on
                 the order button after selecting the items to order.
                 This action is forwarded to the transition function
                 for the order machine to update the list.  Once the
                 list is updated, the selections are also rest.

- Select :: When the user clicks on a list item that is not selected
            already, the state of selections is updated accordingly to
            reflect the new selected index.

- Deselect :: When the user clicks on a list item that is already
              selected the selection state is updated to deselect the
              index.


#+NAME: update
#+BEGIN_SRC elm
update: Action -> State -> State
update action state =
    case action of
        OrderAction m -> 
            { state | lst = orderUpdate m state.lst
                    , selections = NoneSelected
            }
        Select i -> { state | selections = select i state.selections }
        Deselect i -> { state | selections = deselect i state.selections }
#+END_SRC


*** View

The experiment is provided to the user as a browser based application.
The user interface in a browser based application is defined as html
code.  To present the experiment to the user we define a mapping from
the state of the system to html.  The =view= function defines this
mapping.

The =Html= package provides functions to define html tags.  the =div=
function takes two arguments:

- List of Attributes :: The list of html attributes for the tag.
- List of Child Elements :: The list of child elements contained
     inside the tag.

The top level div consists of two elements: 
1. [[List View][listview]] : Html element for the list.
2. [[Order Button][orderButton]] : Html element for the order button.

#+NAME: view
#+BEGIN_SRC elm
view: State -> Html Action
view state =
    Html.div
        []
        [ listView state
        , orderButton state
        ]
#+END_SRC


*** List View

The =listview= function defines the html to display the list.  The
visual representation of the list is defined in a module that provides
common functions for visualizing lists.

The list view needs to trigger actions for selecting/deselecting list
items.  The action that is triggered on a click is defined as an
attribute of the corresponding element and depends on the state of
=selections=.  The =viewList= function takes the list of numbers, list
of selected indices and the action to be triggered when a list item is
clicked.

The Action to be triggered on click is defined as follows:

- If no item is selected :: If no item is selected then we trigger
     =Select= action.

- If one or more items are selected :: If one or more items are
     selected then we trigger =Select= if the item that is clicked is
     not already selected and =Deselect= if the item is already
     selected.


#+NAME: list-view
#+BEGIN_SRC elm
listView: State -> Html Action
listView {lst, selections} =
    let 
        action = 
            ( case selections of 
                NoneSelected -> (\ci -> Select ci)
                OneSelected i -> (\ci -> if ci == i then Deselect ci else Select ci)
                BothSelected i j ->  (\ci -> if ci == i || ci == j then Deselect ci else Select ci)
            )
    in
        Html.div
            []
            [ viewList lst (selectionsList selections) action
            ]
#+END_SRC

*** Order Button

When the user clicks on the order button the =Order= action is
triggered.

#+NAME: order-button
#+BEGIN_SRC elm
orderButton : State -> Html Action
orderButton state = 
    let
        attrlst = if active state then
                      activeStyle state.selections
                  else 
                      inactiveStyle
    in
        Html.div []
            [ Html.button
                (commonStyles ++ attrlst)
                [Html.text "ORDER"]
            ]
#+END_SRC

The html rendering of the order button depends on the state of
selections as follows:

- Two Selected and Out of Order :: If two items are selected and the
     numbers in the selected indices are out of order then the order
     button is =active=.

- Otherwise :: If two indices are not selected or if the selected
               items are in correct order then the button is
               =inactive=.

If the button is active then clicking on the button triggers the
=Order= action in the system.  If the button is inactive then 

#+NAME: order-button-active-inactive
#+BEGIN_SRC elm
inactiveStyle = [ style "background" "#E0E0E0" ]

activeStyle selections =
    case selections of
        BothSelected i j -> 
            [ onClick (OrderAction (Order i j))
            , style "background" "#E0F2F1"
            ]
        _ -> []

commonStyles = 
    [ style "padding" "0.5em"
    , style "font-weight" "bold"
    ]

active: State -> Bool
active state =
    case state.selections of
        BothSelected i j ->
            (Core.outOfOrder i j state.lst)
        _ -> False            
#+END_SRC


* Incrementation Changes for Successive Refinement

To develop a new experiment based on successive refinement of an
already implemented system, we need to make small changes to the
existing system and we can leverage the existing system to build the
new one.

To demonstrate this, we take an example of building the =Adjacent
Order Machine= that is similar the order machine but adds one more
constraint to system.

** From Order Machine to Adjacent Order Machine

*** State

The state space of the basic transition system for Adjacent Order
machine remains the same as that of Order Machine.

#+NAME: adj-order-mc-state
#+BEGIN_SRC elm
type alias AdjOrderMachineState = List Int
#+END_SRC

The state space of the Adjacent Order experiment consists of the state
of the order machine and the state of selections.  Selections are used
to keep track of selected indices.

#+NAME: state-exp-adj
#+BEGIN_SRC elm
type alias State =
    { lst: AdjOrderMachineState
    , selections: Selections
    }
#+END_SRC

*** Actions

Actions in the basic transition system for Adjacent Order Machine are
similar to the actions in the Order machine.  Instead of the =Order=
action here we have =AdjOrder= action.  This action corresponds to
ordering of adjacent items in the list.

|------------+----------------------------------------------------------|
| Action     | Description                                              |
|------------+----------------------------------------------------------|
| AdjOrder i | Perform Adjacent Order operation at index i on the list. |
|------------+----------------------------------------------------------|

#+NAME: adj-order-msg
#+BEGIN_SRC elm
type AdjOrderMachineAction = AdjOrder Int
#+END_SRC

The =Select= and =Deselect= actions are modified to select or deselect
adjacent items in the list.  So, clicking on any item =i= will select
a pair of items =i= and =i+1=.

#+NAME: adj-exp-msg
#+BEGIN_SRC elm
type Action = AdjOrderAction AdjOrderMachineAction
            | Select Int
            | Deselect Int
#+END_SRC

*** Transition Function

The =adjOrderUpdate= function defines the transition function for the
basic transition system of the /Adjacent Order Machine/.  There is
only one action in this machine that corresponds to ordering adjacent
items in the list.

#+NAME: adj-transition
#+BEGIN_SRC elm
adjorderUpdate: AdjOrderMachineAction -> AdjOrderMachineState -> AdjOrderMachineState
adjorderUpdate action state =
    case action of
        AdjOrder i -> adjorder i state
#+END_SRC

The =update= function defines the transition function for the
/Adjacent Order Machine/ experiment.  In addition to the list, this
system also keeps track of the indices where order action is
performed.

#+NAME: adj-exp-update
#+BEGIN_SRC elm
update: Action -> State -> State
update action state =
    case action of
        AdjOrderAction m -> 
            { state | lst = adjorderUpdate m state.lst
                    , selections = NoneSelected 
            }

        Select i -> 
            if i < List.length state.lst - 1 then 
                { state | selections = adjselect i state.selections }
            else state

        Deselect i -> { state | selections = adjdeselect i state.selections }
#+END_SRC


*** View

The html mapping for the experiment consists of a visual mapping for
the list and the button to trigger adjacent order action.

#+NAME: adj-view
#+BEGIN_SRC elm
view: State -> Html Action
view state =
    Html.div
        []
        [ listView state
        , adjorderButton state
        ]
#+END_SRC


**** Adjacent Order Button

The adjacent order button triggers =AdjOrder= action that performs the
ordering operation on the list at selected indices.

The only difference between the =Order= and =AdjOrder= action being
that adjacent ordering as the name suggests performs ordering on
adjacent items in the list.

The selection interface is such that you can only select/deselect
items in adjacent pairs.  Hence the functionality of the adjacent
order button is very much similar to the [[Order Button][order button]] except that
instead of =Order= action we trigger =AdjOrder= action.

#+NAME: adjorder-button
#+BEGIN_SRC elm
adjorderButton : State -> Html Action
adjorderButton state = 
    let
        attrlst = if active state then
                      activeStyle state.selections
                  else 
                      inactiveStyle
    in
        Html.div []
            [ Html.button
                (commonStyles ++ attrlst)
                [Html.text "ORDER"]
            ]
#+END_SRC


#+NAME: adjorder-button-active-inactive
#+BEGIN_SRC elm
inactiveStyle = [ style "background" "#E0E0E0" ]

activeStyle selections =
    case selections of
        BothSelected i j -> 
            [ onClick (AdjOrderAction (AdjOrder i))
            , style "background" "#E0F2F1"
            ]
        _ -> []

commonStyles = 
    [ style "padding" "0.5em"
    , style "font-weight" "bold"
    ]

active: State -> Bool
active state =
    case state.selections of
        BothSelected i j ->
            (Core.outOfOrder i j state.lst)
        _ -> False            
#+END_SRC


* Tangle :noexport:

#+BEGIN_SRC elm :eval no :noweb yes :mkdirp yes :tangle ../build/Order.elm
module Order exposing (..)

import List.Extra as LE
import Html exposing (Html)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Svg
import Svg.Attributes as SA
import Browser

import Core exposing (order)
import Selection exposing (..)
import ListView exposing (..)

<<order-machine-state>>

<<order-mc-action>>

<<transition>>

<<state-exp>>

<<order-exp-action>>

<<update>>

<<view>>

<<list-view>>

<<order-button>>

<<order-button-active-inactive>>


main =
    Browser.sandbox 
        { init = { lst = [10, 30, 5, 20, 6], selections = NoneSelected }
        , view = view
        , update = update
        }

#+END_SRC


#+BEGIN_SRC elm :eval no :mkdirp yes :noweb yes :tangle ../build/AdjOrder.elm
module AdjOrder exposing (..)

import List.Extra as LE
import Html exposing (Html)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Browser

import Core exposing (adjorder)
import Selection exposing (..)
import ListView exposing (..)


<<adj-view>>

<<list-view>>

<<adj-order-mc-state>>

<<state-exp-adj>>

<<adj-transition>>

<<adj-order-msg>>

<<adj-exp-msg>>

<<adj-exp-update>>


<<adjorder-button>>

<<adjorder-button-active-inactive>>
main =
    Browser.sandbox 
        { init = { lst = [2, 10, 30, 5, 50], selections = NoneSelected }
        , view = view
        , update = update
        }

#+END_SRC
