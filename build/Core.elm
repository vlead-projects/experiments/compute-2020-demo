module Core exposing (..)

import List.Extra exposing (swapAt, getAt)


swap : Int -> Int -> List Int -> List Int
swap i j array =
    swapAt i j array

order: Int -> Int -> List Int -> List Int
order i j lst =    
    if (outOfOrder i j lst) then
        swap i j lst
    else
        lst

outOfOrder: Int -> Int -> List Int -> Bool
outOfOrder i j lst =
    let ai = getAt i lst
        aj = getAt j lst
    in
        case (ai, aj) of
            (Just x, Just y) ->
                (i < j) && (x > y)
            _ -> False

adjorder: Int -> List Int -> List Int
adjorder i lst = 
    if i < (List.length lst) - 1 then 
        order i (i + 1) lst 
    else 
        lst
