module Selection exposing (..)

type Selections
    = NoneSelected
    | OneSelected Int
    | BothSelected Int Int

select : Int -> Selections -> Selections
select i selections =
    case selections of
        NoneSelected ->
            OneSelected i
        OneSelected j ->
            BothSelected (min i j) (max i j)
        BothSelected j k ->
            BothSelected j k

deselect : Int -> Selections -> Selections
deselect i selections =
    case selections of

        NoneSelected ->
            NoneSelected

        OneSelected j ->
            if (i == j) then 
                NoneSelected
            else
                OneSelected j

        BothSelected j k ->
            if (i == j) then
                OneSelected k
            else
                if (i == k) then
                    OneSelected j
                else 
                    BothSelected j k

isSelected : Int -> Selections -> Bool
isSelected index selections =
    case selections of
        NoneSelected ->
            False
        OneSelected j ->
            if j == index then True else False
        BothSelected j k ->
            if (j == index || k == index) then True else False

adjselect : Int -> Selections -> Selections
adjselect i selections = 
    selections |> select i |> select (i+1)

adjdeselect : Int -> Selections -> Selections
adjdeselect i selections =
    selections |> deselect i |> deselect (i+1)

selectionsList : Selections -> List Int
selectionsList selections =
    case selections of
       NoneSelected -> []
       OneSelected i -> [i]
       BothSelected i j -> [i, j]
