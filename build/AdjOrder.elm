module AdjOrder exposing (..)

import List.Extra as LE
import Html exposing (Html)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Browser

import Core exposing (adjorder)
import Selection exposing (..)
import ListView exposing (..)


view: State -> Html Action
view state =
    Html.div
        []
        [ listView state
        , adjorderButton state
        ]

listView: State -> Html Action
listView {lst, selections} =
    let 
        action = 
            ( case selections of 
                NoneSelected -> (\ci -> Select ci)
                OneSelected i -> (\ci -> if ci == i then Deselect ci else Select ci)
                BothSelected i j ->  (\ci -> if ci == i || ci == j then Deselect ci else Select ci)
            )
    in
        Html.div
            []
            [ viewList lst (selectionsList selections) action
            ]

type alias AdjOrderMachineState = List Int

type alias State =
    { lst: AdjOrderMachineState
    , selections: Selections
    }

adjorderUpdate: AdjOrderMachineAction -> AdjOrderMachineState -> AdjOrderMachineState
adjorderUpdate action state =
    case action of
        AdjOrder i -> adjorder i state

type AdjOrderMachineAction = AdjOrder Int

type Action = AdjOrderAction AdjOrderMachineAction
            | Select Int
            | Deselect Int

update: Action -> State -> State
update action state =
    case action of
        AdjOrderAction m -> 
            { state | lst = adjorderUpdate m state.lst
                    , selections = NoneSelected 
            }

        Select i -> 
            if i < List.length state.lst - 1 then 
                { state | selections = adjselect i state.selections }
            else state

        Deselect i -> { state | selections = adjdeselect i state.selections }


adjorderButton : State -> Html Action
adjorderButton state = 
    let
        attrlst = if active state then
                      activeStyle state.selections
                  else 
                      inactiveStyle
    in
        Html.div []
            [ Html.button
                (commonStyles ++ attrlst)
                [Html.text "ORDER"]
            ]

inactiveStyle = [ style "background" "#E0E0E0" ]

activeStyle selections =
    case selections of
        BothSelected i j -> 
            [ onClick (AdjOrderAction (AdjOrder i))
            , style "background" "#E0F2F1"
            ]
        _ -> []

commonStyles = 
    [ style "padding" "0.5em"
    , style "font-weight" "bold"
    ]

active: State -> Bool
active state =
    case state.selections of
        BothSelected i j ->
            (Core.outOfOrder i j state.lst)
        _ -> False            
main =
    Browser.sandbox 
        { init = { lst = [2, 10, 30, 5, 50], selections = NoneSelected }
        , view = view
        , update = update
        }
