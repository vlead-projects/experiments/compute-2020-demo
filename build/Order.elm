module Order exposing (..)

import List.Extra as LE
import Html exposing (Html)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Svg
import Svg.Attributes as SA
import Browser

import Core exposing (order)
import Selection exposing (..)
import ListView exposing (..)

type alias OrderMachineState = List Int

type OrderMachineAction = Order Int Int

orderUpdate: OrderMachineAction -> OrderMachineState -> OrderMachineState
orderUpdate action state =
    case action of
        Order i j -> order i j state

type alias State =
    { lst: OrderMachineState
    , selections: Selections
    }

type Action = OrderAction OrderMachineAction
            | Select Int
            | Deselect Int

update: Action -> State -> State
update action state =
    case action of
        OrderAction m -> 
            { state | lst = orderUpdate m state.lst
                    , selections = NoneSelected
            }
        Select i -> { state | selections = select i state.selections }
        Deselect i -> { state | selections = deselect i state.selections }

view: State -> Html Action
view state =
    Html.div
        []
        [ listView state
        , orderButton state
        ]

listView: State -> Html Action
listView {lst, selections} =
    let 
        action = 
            ( case selections of 
                NoneSelected -> (\ci -> Select ci)
                OneSelected i -> (\ci -> if ci == i then Deselect ci else Select ci)
                BothSelected i j ->  (\ci -> if ci == i || ci == j then Deselect ci else Select ci)
            )
    in
        Html.div
            []
            [ viewList lst (selectionsList selections) action
            ]

orderButton : State -> Html Action
orderButton state = 
    let
        attrlst = if active state then
                      activeStyle state.selections
                  else 
                      inactiveStyle
    in
        Html.div []
            [ Html.button
                (commonStyles ++ attrlst)
                [Html.text "ORDER"]
            ]

inactiveStyle = [ style "background" "#E0E0E0" ]

activeStyle selections =
    case selections of
        BothSelected i j -> 
            [ onClick (OrderAction (Order i j))
            , style "background" "#E0F2F1"
            ]
        _ -> []

commonStyles = 
    [ style "padding" "0.5em"
    , style "font-weight" "bold"
    ]

active: State -> Bool
active state =
    case state.selections of
        BothSelected i j ->
            (Core.outOfOrder i j state.lst)
        _ -> False            


main =
    Browser.sandbox 
        { init = { lst = [10, 30, 5, 20, 6], selections = NoneSelected }
        , view = view
        , update = update
        }
